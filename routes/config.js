require('dotenv').config()

var Firebird = require('node-firebird');
const isProduction = process.env.NODE_ENV === 'production'

var options = {};

options.host = process.env.DB_HOST;
options.port = process.env.DB_PORT;
options.database = process.env.DB_DATABASE_CONTABIL;
options.user = process.env.DB_USER;
options.password = process.env.DB_PASSWORD;
options.lowercase_keys = false; // set to true to lowercase keys
options.role = null;            // default
options.pageSize = 8192;        // default when creating database

var pool_contabil = Firebird.pool(5, options);

options.database = process.env.DB_DATABASE_COMPRAS;

var pool_compras = Firebird.pool(5, options);

module.exports = { pool_contabil, pool_compras }