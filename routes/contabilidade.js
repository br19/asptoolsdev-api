const { pool_contabil } = require('./config')
var router = require('express').Router();

const getDespesas = (request, response) => {
  pool_contabil.get(function(err, db) {
 
	if (err)
      throw err;

    var sql = "SELECT * FROM M_ORCDES ";  
    var sql_contador = "SELECT COUNT(0) FROM M_ORCDES ";
    const existingParams = ["PAGE", "SIZE",  "RED_GESTORA", "ANO_VIGENTE", "COD_FICHA", "COD_DESPESA"].filter(field => request.query[field]);
    
    const existingPage = ["PAGE"].filter(field => request.query[field]);
    const existingSize = ["SIZE"].filter(field => request.query[field]);
    const existingFields = ["RED_GESTORA", "ANO_VIGENTE", "COD_FICHA", "COD_DESPESA"].filter(field => request.query[field]);

    sql = " SELECT "
    if (existingSize.length) {
      sql += existingSize.map(field => ` FIRST ${request.query[field]}`);
      var size = existingSize.map(field => request.query[field]);
    }

    var size = request.query.SIZE;

    if (existingPage.length) {
      sql += existingPage.map(field => ` SKIP ${(request.query[field]-1) * size}`);
      var page = existingPage.map(field => request.query[field]);
    }

    sql += " * FROM M_ORCDES"

    if (existingFields.length) {
      sql += " WHERE ";
      sql += existingFields.map(field => `${field} ${field === "COD_DESPESA" ? " CONTAINING ":" = "} ?`).join(" AND ");  

      sql_contador += " WHERE ";
      sql_contador += existingFields.map(field => `${field} ${field === "COD_DESPESA" ? " CONTAINING ":" = "} ?`).join(" AND ");  
      
    }

  sql += " ORDER BY RED_GESTORA, ANO_VIGENTE, COD_FICHA ";

  console.log(sql);
  console.log(sql_contador);

  var contador = 0;
  db.query(sql_contador, existingFields.map(field => request.query[field]), function(err, result, fields) {
    console.log(result);
    contador = result[0].COUNT;
    db.detach();
  });
 
		// db = DATABASE
	db.query(sql, existingFields.map(field => request.query[field]), function(err, result, fields) {
			// IMPORTANT: release the pool_contabil connection
      response.status(200).json({
        "page" : page,
        "totalRecords": contador,
        "despesas": result 
      })
			db.detach();
		});
	});
}

router.get('/despesas?', function(req, res) {
    getDespesas(req, res);
});

module.exports = router;