const { pool_compras } = require('./config')
var router = require('express').Router();

const getProcessos = (request, response) => {
  pool_compras.get(function(err, db) {
   
      if (err)
        throw err;
  
      var sql = "SELECT RED_GESTORA, LIC_S_ANO, LIC_I_PROC FROM COMMVLIC ";  
      var sql_contador = "SELECT COUNT(0) FROM COMMVLIC ";
      const existingParams = ["PAGE", "SIZE",  "RED_GESTORA", "LIC_S_ANO", "LIC_I_PROC",].filter(field => request.query[field]);
      
      const existingPage = ["PAGE"].filter(field => request.query[field]);
      const existingSize = ["SIZE"].filter(field => request.query[field]);
      const existingFields = ["RED_GESTORA", "LIC_S_ANO", "LIC_I_PROC"].filter(field => request.query[field]);
  
      sql = " SELECT "
      if (existingSize.length) {
        sql += existingSize.map(field => ` FIRST ${request.query[field]}`);
        var size = existingSize.map(field => request.query[field]);
      }
  
      var size = request.query.SIZE;
  
      if (existingPage.length) {
        sql += existingPage.map(field => ` SKIP ${(request.query[field]-1) * size}`);
        var page = existingPage.map(field => request.query[field]);
      }
  
      sql += " RED_GESTORA, LIC_S_ANO, LIC_I_PROC FROM COMMVLIC"
  
      if (existingFields.length) {
        sql += " WHERE ";
        sql += existingFields.map(field => `${field} = ?`).join(" AND ");  
  
        sql_contador += " WHERE ";
        sql_contador += existingFields.map(field => `${field} = ?`).join(" AND ");  
        
      }
  
    sql += " ORDER BY RED_GESTORA, LIC_S_ANO, LIC_I_PROC ";
  
    var contador = 0;
    db.query(sql_contador, existingFields.map(field => request.query[field]), function(err, result, fields) {
      contador = result[0].COUNT;
      db.detach();
    });
   
    db.query(sql, existingFields.map(field => request.query[field]), function(err, result, fields) {
        response.status(200).json({
          "page" : page,
          "totalRecords": contador,
          "processos": result 
        })
        db.detach();
      });
    });
  }

  const copiarProcesso = (request, response) => {
    pool_compras.get(function(err, db) {
     
        if (err)
          throw err;
    
        var sql = "SELECT P_COPIA_COMMVLIC.RLIC_I_PROC FROM P_COPIA_COMMVLIC( ";
        
        //FROM P_COPIA_COMMVLIC(:PRED_GESTORA, :PLIC_S_ANO, :PLIC_I_PROC, :PCMS_I_NUME, PLIC_I_PROA, CAST('NOW' AS DATE), :PLIC_S_ANO, '', 1, 1)

        db.query("SELECT RDB$SET_CONTEXT('USER_SESSION','CONFIG.ANO', 2019) FROM RDB$DATABASE", function(err, result, fields) {
          //sdb.detach();
        });

        sql += ` ${request.query.RED_GESTORA}, ${request.query.LIC_S_ANO}, '${request.query.LIC_I_PROC}', 1, 1, CAST('NOW' AS DATE), ${request.query.LIC_S_ANO}, '', 2, 1) `;
        console.log(sql);
        db.query(sql, function(err, result, fields) {
          response.status(200).json(result)
          console.log(result);
          db.detach();
        });
      });
    }
  

router.get('/processos?', function(req, res) {
	getProcessos(req, res);	
});

router.get('/copiarProcesso?', function(req, res) {
	copiarProcesso(req, res);	
});

module.exports = router;