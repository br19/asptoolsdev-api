var express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors')

var app = express();
//const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

app.use('/', require('./routes/index'));
app.use('/contabilidade', require('./routes/contabilidade'))
app.use('/compras', require('./routes/compras'))

// Start server
app.listen(process.env.PORT || 3002, () => {
  console.log(`Server listening`)
})




